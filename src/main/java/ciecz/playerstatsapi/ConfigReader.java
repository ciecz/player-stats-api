package ciecz.playerstatsapi;

import com.google.gson.Gson;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;


public class ConfigReader {

   private static final String DEFAULT_CONFIG = """
         {
         	"apiPort": 8080,
         	"maxSimultaneousConnections": 3,
         	"visibleObjectives": [
         		"hc_playTimeShow",
         		"ts_Deaths"
         	]
         }
         """;
   private static final String FILENAME = "player-stats-api.json";

   public Map<?, ?> config;

   public ConfigReader() throws IOException {
      this.init();
   }

   private void init() throws IOException {
      String configPath = System.getProperty("user.dir") + "/config/";
      this.createDefaults(configPath);
      this.config = this.parseJson(configPath + FILENAME);
   }


   /**
    * Check if the config file exists.
    * If not, creates it with default values.
    */
   private void createDefaults(String configPath) {
      try {
         // First make sure `config` folder exists
         new File(configPath).mkdirs();
         // Then the file
         File configFile = new File(configPath + FILENAME);
         if (!configFile.exists()) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(configFile));
            writer.write(DEFAULT_CONFIG);
            writer.close();
         }
      } catch (IOException exception) {
         PlayerStatsApiMod.LOGGER.error("Could not create the default config file:");
         PlayerStatsApiMod.LOGGER.error(exception.getLocalizedMessage());
      }
   }

   private Map<?, ?> parseJson(String filePath) throws IOException {
      Gson gson = new Gson();
      try (Reader reader = Files.newBufferedReader(Paths.get(filePath))) {
         return gson.fromJson(reader, Map.class);
      }
   }


}
