package ciecz.playerstatsapi;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpServer;
import net.minecraft.scoreboard.ScoreboardObjective;
import net.minecraft.scoreboard.ServerScoreboard;
import net.minecraft.server.MinecraftServer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Based on https://stackoverflow.com/questions/28571086/java-simple-http-server-application-that-responds-in-json/28571398#28571398
 */
public class JsonServer {

   private static final String HEADER_ALLOW = "Allow";
   private static final String HEADER_CONTENT_TYPE = "Content-Type";
   private static final Charset CHARSET = StandardCharsets.UTF_8;

   private static final int STATUS_OK = 200;
   private static final int STATUS_METHOD_NOT_ALLOWED = 405;
   private static final int NO_RESPONSE_LENGTH = -1;

   private static final String METHOD_GET = "GET";
   private static final String METHOD_OPTIONS = "OPTIONS";
   private static final String ALLOWED_METHODS = METHOD_GET + "," + METHOD_OPTIONS;

   private static MinecraftServer serverInstance;
   private static PlayerStatsApiMod modInstance;
   private static int port;
   private static int backlog;

   private HttpServer server;


   public JsonServer(MinecraftServer serverInstance, PlayerStatsApiMod modInstance) throws IOException {
      JsonServer.serverInstance = serverInstance;
      JsonServer.modInstance = modInstance;
      port = (int) ((double) modInstance.CONFIG.config.get("apiPort"));
      backlog = (int) ((double) modInstance.CONFIG.config.get("maxSimultaneousConnections"));
      this.init();
   }


   public void init() throws IOException {
      this.server = HttpServer.create(
            new InetSocketAddress(modInstance.getIp(), port), backlog);

      this.server.createContext("/", ex -> {
         try (ex) {
            final Headers headers = ex.getResponseHeaders();
            final String requestMethod = ex.getRequestMethod().toUpperCase();
            switch (requestMethod) {
               case METHOD_GET -> {
                  // response head
                  StringBuilder responseString = new StringBuilder("{\"status\":\"success\",\"data\":{");
                  // active players + scores
                  responseString.append(this.getJsonActivePlayers());
                  // response tail
                  responseString.append("}}");

                  // set response headers and body
                  final String responseBody = responseString.toString();
                  headers.set(HEADER_CONTENT_TYPE, String.format("application/json; charset=%s", CHARSET));
                  final byte[] rawResponseBody = responseBody.getBytes(CHARSET);
                  // respond
                  ex.sendResponseHeaders(STATUS_OK, rawResponseBody.length);
                  ex.getResponseBody().write(rawResponseBody);
               }
               case METHOD_OPTIONS -> {
                  headers.set(HEADER_ALLOW, ALLOWED_METHODS);
                  ex.sendResponseHeaders(STATUS_OK, NO_RESPONSE_LENGTH);
               }
               default -> {
                  headers.set(HEADER_ALLOW, ALLOWED_METHODS);
                  ex.sendResponseHeaders(STATUS_METHOD_NOT_ALLOWED, NO_RESPONSE_LENGTH);
               }
            }
         }
      });
      this.server.start();
   }


   /**
    * For each online player it checks if they have objectives that are defined in the config file,
    * and if so it adds their scores to the output.
    *
    * @return a JSON string with online players and their scores; if there are no online players, returns "players": []
    */
   private @NotNull String getJsonActivePlayers() {
      List<String> onlinePlayers = Arrays.stream(serverInstance.getPlayerNames()).toList();
      final int onlinePlayersCount = onlinePlayers.size();
      StringBuilder returnString = new StringBuilder("\"players\":[");
      if (onlinePlayersCount > 0) {
         ServerScoreboard scoreboard = serverInstance.getScoreboard();
         //noinspection unchecked TODO more robust verification of file before loading
         ArrayList<String> visibleObjectives = (ArrayList<String>) modInstance.CONFIG.config.get("visibleObjectives");

         onlinePlayers.forEach(player -> {
            returnString.append("{\"name\": \"").append(player).append("\",");
            returnString.append("\"scores\": {");
            boolean anyObjectives = false;
            for (String objectiveName : visibleObjectives) {
               ScoreboardObjective objective = scoreboard.getNullableObjective(objectiveName);
               if (scoreboard.playerHasObjective(player, objective)) {
                  anyObjectives = true;
                  returnString.append("\"").append(objectiveName).append("\":\"")
                        .append(scoreboard.getPlayerScore(player, objective).getScore()).append("\",");
               }
            }
            if (anyObjectives) {
               returnString.setLength(returnString.length() - 1); // remove trailing comma
            }
            returnString.append("}},");
         });

         returnString.setLength(returnString.length() - 1); // remove trailing comma
      }
      returnString.append("]");
      return returnString.toString();
   }


   public static int getPort() {
      return port;
   }


   public void stop() {
      this.server.stop(0);
   }


}
