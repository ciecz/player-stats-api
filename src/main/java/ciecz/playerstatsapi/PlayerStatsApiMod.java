package ciecz.playerstatsapi;

import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@Environment(EnvType.SERVER)
public class PlayerStatsApiMod implements DedicatedServerModInitializer {

   public static final String MOD_ID = "player-stats-api";
   public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);
   public ConfigReader CONFIG;

   public JsonServer jsonServer;
   private String ip;


   @Override
   public void onInitializeServer() {
      ServerLifecycleEvents.SERVER_STARTED.register(server -> {
         // If server.getServerIp() returns an empty string, it means you didn't set an IP
         // in server.properties file, which binds it to all available IPs.
         // In that case, we use 0.0.0.0 to bind to all interfaces.
         this.ip = server.getServerIp().isEmpty() ? "0.0.0.0" : server.getServerIp();
         try {
            this.CONFIG = new ConfigReader();
            this.jsonServer = new JsonServer(server, this);
            LOGGER.info("API started on http://" + this.ip + ":" + JsonServer.getPort() + "/");
         } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage());
         }
      });

      ServerLifecycleEvents.SERVER_STOPPING.register(server -> this.jsonServer.stop());
   }


   public @NotNull String getIp() {
      return this.ip;
   }


}
