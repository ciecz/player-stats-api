<div align="center">

# Player Stats API

[**Introduction**](#introduction) •
[**Example Response**](#example-response) •
[**Requirements**](#requirements) •
[**Config**](#config) •
[**Building**](#building) •
[**Download**](#download) •
[**License & attributions**](#license--attributions)

[![Minecraft Version](https://img.shields.io/badge/minecraft%20version-1.20.2-blueviolet)](#)
[![Fabric API](https://img.shields.io/badge/fabric%20api-0.91.1-red)](https://modrinth.com/mod/fabric-api)
[![License](https://img.shields.io/badge/license-MIT-green)](LICENSE)
</div>


## Introduction

Player Stats API is a **server-side only** Fabric mod. When installed, it starts a simple HTTP server that sends JSON responses whenever pinged. It responds only to incoming `GET` requests.

The response includes a list of online players and their scoreboard scores. The objectives to be shown can be customized in a config file.

## Example Response

### Browser

The easiest way to see the response is to use a web browser.

Go to your server IP and the API port (`8080` by default). If you're running a local server, it will probably be `127.0.0.1:8080`.

An example response with 2 players online:

![](./img/example-browser.png)

Player 'cieczu' has 2 objectives set: 'hc_playTimeShow' = 3, and 'ts_Deaths' = 1.

Player 'Notch' only has 1 objective: 'hc_playTimeShow' = 0.

### Command line

You can use any HTTP capable tools, like `wget` or `curl`. Below is an example with curl:


![](./img/example-curl.png)


## Requirements

This mod requires:
- [Fabric API](https://modrinth.com/mod/fabric-api)

## Config

After the first run, it generates a config file in `config/` server directory.

In there you can change:
- the API port (default: `8080`);
- objectives to show (default: `hc_playTimeShow` and `ts_Deaths` from [Vanilla Tweaks "Track Statistics" and "Track Raw Statistics" datapacks](https://vanillatweaks.net/picker/datapacks/));
- limit on maximum allowed connections at the same time (default: `3`).

## Building

### Using command line

1. Clone the repo  `git clone https://gitlab.com/ciecz/player-stats-api/`
2. For the executable, use `gradlew.bat` if you're on Windows, or `gradlew` on Linux/macOS/BSD. Examples below use Linux.
3. Generate the jar file using `./gradlew build` This should generate sources and build the project automatically first.
4. Go to `build/libs/` and copy the mod jar into `mods/` directory of your server. Make sure to install mods from [Requirements](#requirements)!

### Using IntelliJ

1. Go to `File -> New -> Project from Version Control...`
2. Use `https://gitlab.com/ciecz/player-stats-api/` for the URL and press `Clone`.
3. Wait until Gradle finishes building the project.
4. Use Gradle tasks panel on the right-hand side. To build jar, go to `player-stats-api -> Tasks -> build -> build`.
5. After the task finishes, in file browser on the left-hand side go to `player-stats-api/build/libs/` and copy the jar file into `mods/` directory of your server. Make sure to install mods from [Requirements](#requirements)!

Alternatively, if you're editing the code and want to test-run, use `Tasks -> fabric -> runServer`.

This runs an integrated server with console inside IntelliJ, and automatically downloads all the required mods.

## Download 

Gitlab releases coming soon™️. For now, use [Artifacts](https://gitlab.com/ciecz/player-stats-api/-/tags) to download both jar file and sources. Available for Minecraft 1.20.2+

For older releases, clone project and rollback to older commits, and build using instructions in [Building](#building).

## License & attributions

The project is licensed under the [MIT license](LICENSE).

The HTTP server code is based on xehpuk on stackoverflow.com (https://stackoverflow.com/questions/28571086/java-simple-http-server-application-that-responds-in-json/28571398#28571398).
